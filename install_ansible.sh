#!/bin/bash -eu

# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

apt update && apt install -y software-properties-common

apt-add-repository --yes --update ppa:ansible/ansible

cat > /etc/apt/preferences.d/ppa_ansible.pref << EOF
# Use PPA only for Ansible packages
Package: *
Pin: origin "ppa.launchpad.net/ansible"
Pin-Priority: -10

Package: ansible
Pin: origin "ppa.launchpad.net/ansible"
Pin-Priority: 900

EOF

apt update && apt install -y ansible sudo
